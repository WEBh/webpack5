const { resolve, join } = require('path')
const glob = require('glob')
const PATHS = { src: join(__dirname,'src') }
const HtmlWebpackPlugin = require('html-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const { CleanWebpackPlugin } = require('clean-webpack-plugin')
const OptimizeCssAssetsWebpackPlugin = require("optimize-css-assets-webpack-plugin")
const PurgecssWebpackPlugin = require('purgecss-webpack-plugin')

module.exports = {
    /* 1. entry入口(entry)指webpack以哪个文件作为入口起点开始打包**/
    entry: './src/main.js',
    //输出文件
    output: {
        filename: 'bulid.js',
        path: resolve(__dirname, 'dist')
    },

    module: {
        rules: [{
                test: /\.css$/,
                use: [MiniCssExtractPlugin.loader, 'css-loader', 'postcss-loader']
            },
            {
                test: /\.scss$/,
                use: [MiniCssExtractPlugin.loader, 'css-loader', 'sass-loader', 'postcss-loader']
            },
            {
                test: /\.(png|jpg|jpeg|gif|svg)$/,
                loader: 'url-loader',
                options: {
                    publicPath: './images',
                    outputPath: 'images/',
                    limit: 1024 * 8, //小于等于8k转换base64格式
                    name: '[name][hash:10].[ext]'
                }
            },
           
            {
                exclude: /\.(js|json|html|css|less|scss|styl|png|gif|jpg|jpeg)$/,
                loader: 'file-loader',
                options: {
                    outputPath: 'font/',
                    publicPath: './font',
                    name: '[name][hash:10].[ext]'
                }
            },
            {
                test: /\.html$/i,
                loader: 'html-loader'
            },
            {
                test: /\.js$/,
                exclude: /node_modules/,
                loader: "eslint-loader",
                options: {
                    fix: true
                }
            }
        ]
    },

    plugins: [
        new HtmlWebpackPlugin({
            template: "./src/index.html",
            // filename:'main.html',
            minify: {
                collapseWhitespace: true,
                removeComments: true
            }
        }),
        //单独提取css文件
        new MiniCssExtractPlugin({
            filename: 'index.css'
        }),
        //压缩css
        new OptimizeCssAssetsWebpackPlugin(),
        // 去除无用的css
        new PurgecssWebpackPlugin({
            paths: glob.sync(`${PATHS.src}/**/*`,{ nodir: true })
        }),
        new CleanWebpackPlugin()
    ],

    target: "web",
    // mode: "development" //production 生产模式

}