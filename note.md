# webpack5

```
// entry 多文件入口写法 
entry:['./src/index.js','./src/main.js'],  //合成一个chunk
entry:{
   one: './src/index.js',
   two: './src/main.js'
}
entry:{
   vendor: ['./src/jquery.js','./src/common.js']
   index: './src/index.js',
   cart: './src/cart.js'
}
 //输出文件
output:{
    filename: '[name].js',
    path:resolve(__dirname, 'build')
},


//多html使用
 plugins:[
    new HtmlWebpackPlugin({
        template: "./src/index.html",
        filename:'demo.html',
        chunks:["index","vendor"] //指定js文件
        minify:{
             collapseWhitespace:true,
             removeComments:true
        }
    }),
     new HtmlWebpackPlugin({
        template: "./src/cart.html",
        filename:'demo.html',
        chunks:["cart","vendor"] //指定js文件
        minify:{
             collapseWhitespace:true,
             removeComments:true
        }
    })
],
```